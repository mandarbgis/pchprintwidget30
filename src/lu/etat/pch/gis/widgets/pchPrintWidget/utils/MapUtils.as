package lu.etat.pch.gis.widgets.pchPrintWidget.utils
{
	import com.esri.ags.Graphic;
	import com.esri.ags.Map;
	import com.esri.ags.geometry.Extent;
	import com.esri.ags.geometry.MapPoint;
	import com.esri.ags.layers.*;
	import com.esri.ags.virtualearth.VETiledLayer;
	
	import lu.etat.pch.gis.widgets.pchPrintWidget.PchJSONUtils;
	import lu.etat.pch.gis.widgets.pchPrintWidget.json.PchPrintMapService;
	
	import mx.collections.ArrayCollection;

	public class MapUtils
	{
		public static function getMapUnits(map:Map):String {
			var mapLayers:ArrayCollection = map.layers as ArrayCollection;
			if (mapLayers != null && mapLayers.length > 0) {
				var firstLayer:Layer =mapLayers.getItemAt(0) as Layer;
				var mapUnits:String="??";
				if(firstLayer is ArcGISTiledMapServiceLayer) mapUnits = ArcGISTiledMapServiceLayer(firstLayer).units;  
				else if(firstLayer is ArcGISDynamicMapServiceLayer) mapUnits = ArcGISDynamicMapServiceLayer(firstLayer).units;  
				else if(firstLayer is ArcIMSMapServiceLayer) mapUnits = ArcIMSMapServiceLayer(firstLayer).units;  
				else if(firstLayer is OpenStreetMapLayer) mapUnits = OpenStreetMapLayer(firstLayer).units;
				else if(firstLayer is VETiledLayer) mapUnits = VETiledLayer(firstLayer).units;
				return mapUnits;
			}
			return null;
		}
		
		public static function calculateExtent(anchorPoint:MapPoint, anchor:String, height:Number, width:Number):Extent {		
			var extent:Extent = new Extent(anchorPoint.x,anchorPoint.y,anchorPoint.x+height,anchorPoint.y+width);
			return extent;
		}
		
		public static function getPrintLayers(map:Map,printGraphicsLayer:GraphicsLayer):Object {
			var toPrintMapServices:ArrayCollection=new ArrayCollection();
			var toPrintGraphics:ArrayCollection=new ArrayCollection();
			var mapLayers:ArrayCollection=map.layers as ArrayCollection;
			for(var i:int=0; i<mapLayers.length; i++) {
				var layer:Layer=mapLayers[i];
				if(layer.loaded && layer.visible) {
					if((layer is GraphicsLayer)&&!(layer is FeatureLayer)) {
						var tmpGraphicsLayer:GraphicsLayer=layer as GraphicsLayer;
						if(tmpGraphicsLayer!=printGraphicsLayer) {
							var drawGraphics:ArrayCollection=tmpGraphicsLayer.graphicProvider as ArrayCollection;
							for(var j:int=0; j<drawGraphics.length; j++) {
								var drawGraphic:Graphic=drawGraphics.getItemAt(j) as Graphic;
								toPrintGraphics.addItem(PchJSONUtils.encodeGraphic(drawGraphic));
							}
						}
					} else {
						var mapService:PchPrintMapService=PchPrintMapService.getMapServiceFromLayer(layer);
						if(mapService) {
							toPrintMapServices.addItem(mapService);
						}
					} 	
				}
			}
			var object:Object={ printMapServices:toPrintMapServices, printGraphicsLayer:toPrintGraphics }
			return object;
		}
		
		public static function isExtentOutOfMap(map:Map,extent:Extent, mapExtent:Extent):Boolean {
			var outOfMapExtent:Boolean=false;
			if(map) {
				var bottomOfHeader:MapPoint=map.toMapFromStage(0, 40); // MapPoint corresponding to the bottom of the header
				if(bottomOfHeader&&map.extent&&extent) {
					if(extent.xmin<map.extent.xmin||extent.xmax>map.extent.xmax||extent.ymin<map.extent.ymin||extent.ymax>bottomOfHeader.y) //map.extent.ymax)
						outOfMapExtent=true;
				}
			}
			return outOfMapExtent;
		}
	}
}