package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    import com.esri.ags.symbols.SimpleLineSymbol;
    /**
     *
     * @author schullto
     */
    public class AgsJsonSimpleLineSymbol extends AgsJsonSymbol {

        /**
         *
         * @param style
         * @param color
         * @param width
         */
        public function AgsJsonSimpleLineSymbol(style:String="solid", color:AgsJsonRGBColor=null, width:Number=1) {
            type="esriSLS";
            this.style=style;
            this.color=color;
            this.width=width;
        }

        private static var validStyles:Array=[ "esriSLSSolid", "esriSLSDash", "esriSLSDot", "esriSLSDashDot", "esriSLSDashDotDot", "esriSLSNull", "esriSLSInsideFrame" ];

        public static function getFromObject(sls:SimpleLineSymbol):AgsJsonSimpleLineSymbol {
            if(sls==null)
                return null;
            var color:AgsJsonRGBColor=AgsJsonRGBColor.getObjectFromUINT(sls.color, sls.alpha);
            var retSym:AgsJsonSimpleLineSymbol=new AgsJsonSimpleLineSymbol(sls.style, color, sls.width);
            return retSym;
        }

        private var _color:AgsJsonRGBColor;
        private var _style:String;
        private var _width:Number;

        /**
         *
         * @return
         */
        public function get color():AgsJsonRGBColor {
            return _color;
        }

        /**
         *
         * @param value
         */
        public function set color(value:AgsJsonRGBColor):void {
            _color=value;
        }

        /**
         *     esriSLSSolid = "solid";
         *     esriSLSDash = "dash";
         *     esriSLSDot = "dot";
         *     esriSLSDashDot = "dashdot";
         *     esriSLSDashDotDot = "dashdotdot;
         *     esriSLSNull = "null";
         *     esriSLSInsideFrame = "insideFrame";
         * @return
         */
        public function get style():String {
            return _style;
        }

        /**
         *     esriSLSSolid = "solid";
         *     esriSLSDash = "dash";
         *     esriSLSDot = "dot";
         *     esriSLSDashDot = "dashdot";
         *     esriSLSDashDotDot = "dashdotdot;
         *     esriSLSNull = "null";
         *     esriSLSInsideFrame = "insideFrame";
         * @param value
         */
        public function set style(value:String):void {
            _style=type+value.substring(0, 1).toUpperCase()+value.substring(1).toLowerCase();
            if(_style=="esriSLSDashdot")
                _style="esriSLSDashDot";
            if(_style=="esriSLSDashdotdot")
                _style="esriSLSDashDotDot";
            if(_style=="esriSLSInsideframe")
                _style="esriSLSInsideFrame";
            if(validStyles.indexOf(_style)<0) {
                trace("!!!!!! invalid style used for AgsJsonSimpleLineSymbol: "+value+" !!!!!!");
            }
        }

        /**
         *
         * @return
         */
        public function get width():Number {
            return _width;
        }

        /**
         *
         * @param value
         */
        public function set width(value:Number):void {
            _width=value;
        }
    }
}