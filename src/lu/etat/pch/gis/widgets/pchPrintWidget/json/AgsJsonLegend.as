package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    public class AgsJsonLegend extends AgsJsonSymbol {
        public function AgsJsonLegend(legendTitle:String="Legend", legendColumns:Number=1, includeAllLayers:Boolean=true, exceptionLayerNames:Array=null, exceptionLayerNamesStr:String="", showLayerNames:Boolean=true, showLabels:Boolean=true, showHeadings:Boolean=false, legendBackground:AgsJsonSimpleFillSymbol=null) {
            type="pchLegend";
            this.legendTitle=legendTitle;
            this.legendColumns=legendColumns;
            this.includeAllLayers=includeAllLayers;
            this.exceptionLayerNames=exceptionLayerNames;
			this.exceptionLayerNamesStr=exceptionLayerNamesStr;
            this.showLayerNames=showLayerNames;
            this.showLabels=showLabels;
            this.showHeadings=showHeadings;
            this.legendBackground=legendBackground;			
        }

        private var _exceptionLayerNames:Array;
        private var _includeAllLayers:Boolean;
        private var _legendBackground:AgsJsonSimpleFillSymbol;
        private var _legendColumns:Number;
        private var _legendTitle:String;
        private var _showHeadings:Boolean;
        private var _showLabels:Boolean;
        private var _showLayerNames:Boolean;
		private var _exceptionLayerNamesStr:String;
		private var _legendFormat:AgsJsonLegendFormat;
		private var _rightToLeft:Boolean;
		private var _defaultLegendClassFormat:AgsJsonLegendClassFormat;
		private var _defaultHeadingSymbol:AgsJsonTextSymbol;
		private var _defaultLayerNameSymbol:AgsJsonTextSymbol;


		public function get legendFormat():AgsJsonLegendFormat
		{
			return _legendFormat;
		}

		public function set legendFormat(value:AgsJsonLegendFormat):void
		{
			_legendFormat = value;
		}

        public function get exceptionLayerNames():Array {
            return _exceptionLayerNames;
        }

        public function set exceptionLayerNames(value:Array):void {
            _exceptionLayerNames=value;
        }
		
		public function get exceptionLayerNamesStr():String {
			return _exceptionLayerNamesStr;
		}
		
		public function set exceptionLayerNamesStr(value:String):void {
			_exceptionLayerNamesStr=value;
			_exceptionLayerNames=value.split(",");
		}

        public function get includeAllLayers():Boolean {
            return _includeAllLayers;
        }

        public function set includeAllLayers(value:Boolean):void {
            _includeAllLayers=value;
        }

        public function get legendBackground():AgsJsonSimpleFillSymbol {
            return _legendBackground;
        }

        public function set legendBackground(value:AgsJsonSimpleFillSymbol):void {
            _legendBackground=value;
        }

        public function get legendColumns():Number {
            return _legendColumns;
        }

        public function set legendColumns(value:Number):void {
            _legendColumns=value;
        }

        public function get legendTitle():String {
            return _legendTitle;
        }

        public function set legendTitle(value:String):void {
            _legendTitle=value;
        }

        public function get showHeadings():Boolean {
            return _showHeadings;
        }

        public function set showHeadings(value:Boolean):void {
            _showHeadings=value;
        }

        public function get showLabels():Boolean {
            return _showLabels;
        }

        public function set showLabels(value:Boolean):void {
            _showLabels=value;
        }

        public function get showLayerNames():Boolean {
            return _showLayerNames;
        }

        public function set showLayerNames(value:Boolean):void {
            _showLayerNames=value;
        }

		public function get rightToLeft():Boolean
		{
			return _rightToLeft;
		}

		public function set rightToLeft(value:Boolean):void
		{
			_rightToLeft = value;
		}

		public function get defaultLegendClassFormat():AgsJsonLegendClassFormat
		{
			return _defaultLegendClassFormat;
		}

		public function set defaultLegendClassFormat(value:AgsJsonLegendClassFormat):void
		{
			_defaultLegendClassFormat = value;
		}

		public function get defaultHeadingSymbol():AgsJsonTextSymbol
		{
			return _defaultHeadingSymbol;
		}

		public function set defaultHeadingSymbol(value:AgsJsonTextSymbol):void
		{
			_defaultHeadingSymbol = value;
		}

		public function get defaultLayerNameSymbol():AgsJsonTextSymbol
		{
			return _defaultLayerNameSymbol;
		}

		public function set defaultLayerNameSymbol(value:AgsJsonTextSymbol):void
		{
			_defaultLayerNameSymbol = value;
		}
    }
}