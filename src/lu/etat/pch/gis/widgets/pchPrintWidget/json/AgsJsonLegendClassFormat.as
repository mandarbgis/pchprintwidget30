package lu.etat.pch.gis.widgets.pchPrintWidget.json
{
	public class AgsJsonLegendClassFormat
	{
		private var _labelSymbol:AgsJsonTextSymbol;
		private var _descriptionSymbol:AgsJsonTextSymbol;
		private var _patchWidth:Number;
		private var _patchHeight:Number;

		public function AgsJsonLegendClassFormat()
		{
		}

		public function get labelSymbol():AgsJsonTextSymbol
		{
			return _labelSymbol;
		}

		public function set labelSymbol(value:AgsJsonTextSymbol):void
		{
			_labelSymbol = value;
		}

		public function get descriptionSymbol():AgsJsonTextSymbol
		{
			return _descriptionSymbol;
		}

		public function set descriptionSymbol(value:AgsJsonTextSymbol):void
		{
			_descriptionSymbol = value;
		}

		public function get patchWidth():Number
		{
			return _patchWidth;
		}

		public function set patchWidth(value:Number):void
		{
			_patchWidth = value;
		}

		public function get patchHeight():Number
		{
			return _patchHeight;
		}

		public function set patchHeight(value:Number):void
		{
			_patchHeight = value;
		}


	}
}