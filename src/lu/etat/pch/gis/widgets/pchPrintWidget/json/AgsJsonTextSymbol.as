package lu.etat.pch.gis.widgets.pchPrintWidget.json {
	import com.esri.ags.symbols.TextSymbol;

    public class AgsJsonTextSymbol extends AgsJsonSymbol {
        public function AgsJsonTextSymbol(text:String=null, textFormat:AgsJsonTextFormat=null, color:AgsJsonRGBColor=null, borderLineColor:AgsJsonRGBColor=null, angle:Number=0, xoffset:Number=0, yoffset:Number=0) {
            super.type="esriTS";
            this._text=text;
			this._textFormat=textFormat;
            this._color=color;
            this._borderLineColor=borderLineColor;
            this._angle=angle;
            this._xoffset=xoffset;
			this._yoffset=yoffset;
        }
		
		/*todo: implement me
		symbolObj.verticalAlignment
		symbolObj.horizontalAlignment
		symbolObj.rightToLeft
		symbolObj.kerning
		*/

		public static function getObjectFromTextSymbol(ts:TextSymbol):AgsJsonTextSymbol {
			if(ts==null)
				return null;
			var text:String = ts.text;
			var textFormat:AgsJsonTextFormat=AgsJsonTextFormat.getObjectFromTextFormat(ts.textFormat);
			var color:AgsJsonRGBColor=AgsJsonRGBColor.getObjectFromUINT(ts.color, ts.alpha);
			var borderLineColor:AgsJsonRGBColor=AgsJsonRGBColor.getObjectFromUINT(ts.borderColor);
			var angle:Number=ts.angle;
			var xoffset:Number=ts.xoffset;
			var yoffset:Number=ts.yoffset;
			var retSym:AgsJsonTextSymbol=new AgsJsonTextSymbol(text,textFormat,color,borderLineColor,angle,xoffset,yoffset);
			if(ts.background) {
				var backgroundColor:AgsJsonRGBColor=AgsJsonRGBColor.getObjectFromUINT(ts.backgroundColor);
				retSym.backgroundColor=backgroundColor;
			}
			return retSym;
		}

        private var _angle:Number;
        private var _borderLineColor:AgsJsonRGBColor;
		private var _color:AgsJsonRGBColor;
		private var _backgroundColor:AgsJsonRGBColor;
        private var _textFormat:AgsJsonTextFormat;
        private var _text:String;
        private var _xoffset:Number;
        private var _yoffset:Number;

		public function get angle():Number {
            return _angle;
        }

        public function set angle(value:Number):void {
            _angle=value;
        }

        public function get borderLineColor():AgsJsonRGBColor {
            return _borderLineColor;
        }

        public function set borderLineColor(value:AgsJsonRGBColor):void {
            _borderLineColor=value;
        }

        public function get color():AgsJsonRGBColor {
            return _color;
        }

        public function set color(value:AgsJsonRGBColor):void {
            _color=value;
        }

		
        public function get textFormat():AgsJsonTextFormat {
            return _textFormat;
        }

        public function set textFormat(value:AgsJsonTextFormat):void {
			_textFormat=value;
        }

        public function get text():String {
            return _text;
        }

        public function set text(value:String):void {
            _text=value;
        }

        public function get xoffset():Number {
            return _xoffset;
        }

        public function set xoffset(value:Number):void {
            _xoffset=value;
        }

        public function get yoffset():Number {
            return _yoffset;
        }

        public function set yoffset(value:Number):void {
            _yoffset=value;
        }

		public function get backgroundColor():AgsJsonRGBColor
		{
			return _backgroundColor;
		}

		public function set backgroundColor(value:AgsJsonRGBColor):void
		{
			_backgroundColor = value;
		}

    }
}