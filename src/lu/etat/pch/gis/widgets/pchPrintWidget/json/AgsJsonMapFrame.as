package lu.etat.pch.gis.widgets.pchPrintWidget.json
{
	import com.esri.ags.geometry.Extent;
	
	
	public class AgsJsonMapFrame extends AgsJsonSymbol {
		public function AgsJsonMapFrame(extent:Extent=null,mapServices:Array=null,mapElements:Array=null) {
			type="pchMapFrame";
			this.extent=extent;
			this.mapServices=mapServices;
			this.mapElements=mapElements;
		}
		
		private var _extent:Extent;
		private var _mapServices:Array;
		private var _mapElements:Array;
		private var _border:AgsJsonSimpleLineSymbol;
		private var _referenceScale:Number;
		private var _rotation:Number;
		
		public function get extent():Extent {
			return _extent;
		}
		
		public function set extent(value:Extent):void {
			_extent = value;
		}
		
		public function get mapServices():Array {
			return _mapServices;
		}
		
		public function set mapServices(value:Array):void {
			_mapServices = value;
		}
		
		public function get mapElements():Array {
			return _mapElements;
		}
		
		public function set mapElements(value:Array):void {
			_mapElements = value;
		}

		public function get border():AgsJsonSimpleLineSymbol
		{
			return _border;
		}

		public function set border(value:AgsJsonSimpleLineSymbol):void
		{
			_border = value;
		}

		public function get referenceScale():Number
		{
			return _referenceScale;
		}

		public function set referenceScale(value:Number):void
		{
			_referenceScale = value;
		}

		public function get rotation():Number
		{
			return _rotation;
		}

		public function set rotation(value:Number):void
		{
			_rotation = value;
		}


	}
	
}