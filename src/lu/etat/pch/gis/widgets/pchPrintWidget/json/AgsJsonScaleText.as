package lu.etat.pch.gis.widgets.pchPrintWidget.json {
	import com.esri.ags.symbols.TextSymbol;

    /**
     *
     * @author schullto
     */
    public class AgsJsonScaleText extends AgsJsonSymbol {
        /**
         *
         * @param style
         * @param mapUnitLabel
         * @param pageUnitLabel
         * @param numberFormat
         * @param mapUnits
         * @param pageUnits
         */
        public function AgsJsonScaleText(style:Number=0, mapUnitLabel:String="", pageUnitLabel:String="", numericFormat:AgsJsonNumericFormat=null, mapUnits:String="centimeters", pageUnits:String="meters", separator:String=":") {
            type="pchScaleText";
            this.style=style;
            this.numericFormat=numericFormat;
            if(numericFormat==null)
                this.numericFormat=new AgsJsonNumericFormat(0, 3)
            this.mapUnitLabel=mapUnitLabel;
            this.pageUnitLabel=pageUnitLabel;
            this.mapUnits=mapUnits;
            this.pageUnits=pageUnits;
            this.separator=separator;
			this.backgroundColor=null;
        }

        private var _mapUnitLabel:String;
        private var _mapUnits:String;
        private var _numericFormat:AgsJsonNumericFormat;
        private var _pageUnitLabel:String;
        private var _pageUnits:String;
        private var _separator:String;
        private var _style:Number;
		private var _backgroundColor:AgsJsonRGBColor;

        /**
         *
         * @return
         */
        public function get mapUnitLabel():String {
            return _mapUnitLabel;
        }

        /**
         *
         * @param value
         */
        public function set mapUnitLabel(value:String):void {
            _mapUnitLabel=value;
        }

        /**
         * inches
         * points
         * feets
         * yards
         * miles
         * nauticalmiles
         * millimeters
         * centimeters
         * meters
         * kilometers
         * degrees
         * decimeters
         * @return
         */
        public function get mapUnits():String {
            return _mapUnits;
        }

        /**
         * inches
         * points
         * feets
         * yards
         * miles
         * nauticalmiles
         * millimeters
         * centimeters
         * meters
         * kilometers
         * degrees
         * decimeters
         * @param value
         */
        public function set mapUnits(value:String):void {
            _mapUnits=value;
        }

        /**
         *
         * @return
         */
        public function get numericFormat():AgsJsonNumericFormat {
            return _numericFormat;
        }

        /**
         *
         * @param value
         */
        public function set numericFormat(value:AgsJsonNumericFormat):void {
            _numericFormat=value;
        }

        /**
         *
         * @return
         */
        public function get pageUnitLabel():String {
            return _pageUnitLabel;
        }

        /**
         *
         * @param value
         */
        public function set pageUnitLabel(value:String):void {
            _pageUnitLabel=value;
        }

        /**
         * inches
         * points
         * feets
         * yards
         * miles
         * nauticalmiles
         * millimeters
         * centimeters
         * meters
         * kilometers
         * degrees
         * decimeters
         * @return
         */
        public function get pageUnits():String {
            return _pageUnits;
        }

        /**
         * inches
         * points
         * feets
         * yards
         * miles
         * nauticalmiles
         * millimeters
         * centimeters
         * meters
         * kilometers
         * degrees
         * decimeters
         * @param value
         */
        public function set pageUnits(value:String):void {
            _pageUnits=value;
        }

        public function get separator():String {
            return _separator;
        }

        public function set separator(value:String):void {
            _separator=value;
        }

        /**
         * sriScaleTextStyleEnum:
         *    int esriScaleTextAbsolute = 0;
         *    int esriScaleTextRelative = 1;
         *    int esriScaleTextCustom = 2;
         * @return
         */
        public function get style():Number {
            return _style;
        }

        /**
         * sriScaleTextStyleEnum:
         *    int esriScaleTextAbsolute = 0;
         *    int esriScaleTextRelative = 1;
         *    int esriScaleTextCustom = 2;
         * @param value
         */
        public function set style(value:Number):void {
            _style=value;
        }
		
		
		public function get backgroundColor():AgsJsonRGBColor
		{
			return _backgroundColor;
		}

		public function set backgroundColor(value:AgsJsonRGBColor):void
		{
			_backgroundColor = value;
		}


    }
}