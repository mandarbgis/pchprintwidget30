package lu.etat.pch.gis.widgets.pchPrintWidget.json
{
	public class AgsJsonLegendFormat
	{
		public function	AgsJsonLegendFormat(defaultPatchWidth:Number=0,defaultPatchHeight:Number=0,
											groupGap:Number=0,headingGap:Number=0,horizontalItemGap:Number=0,
											horizonalPatchGab:Number=0,layerNameGap:Number=0,
											showTitle:Boolean=true,textGab:Number=0.5,titleGab:Number=1,
											titleSymbol:AgsJsonTextSymbol=null,verticalItemGap:Number=0,verticalPatchGap:Number=0)		{
			this._defaultPatchHeight = defaultPatchHeight;
			this._defaultPatchWidth = defaultPatchWidth;
			this._groupGap = groupGap;
			this._headingGap = headingGap;
			this._horizontalItemGap = horizontalItemGap;
			this._horizontalPatchGab = horizontalPatchGab;
			this._layerNameGap = layerNameGap;
			this._showTitle = showTitle;
			this._textGab = textGab;
			this._titleGab = titleGab;
			this._titleSymbol = titleSymbol;
			this._verticalItemGap = verticalItemGap;
			this._verticalPatchGap = verticalPatchGap;
			
		}
		private var _titleSymbol:AgsJsonTextSymbol;
		private var _showTitle:Boolean;
		private var _titleGab:Number;
		private var _textGab:Number;
		private var _defaultPatchHeight:Number;
		private var _defaultPatchWidth:Number;
		private var _groupGap:Number;
		private var _headingGap:Number;
		private var _horizontalItemGap:Number;
		private var _horizontalPatchGab:Number;
		private var _layerNameGap:Number;
		private var _verticalItemGap:Number;
		private var _verticalPatchGap:Number;

		public function get verticalPatchGap():Number
		{
			return _verticalPatchGap;
		}

		public function set verticalPatchGap(value:Number):void
		{
			_verticalPatchGap = value;
		}

		public function get verticalItemGap():Number
		{
			return _verticalItemGap;
		}

		public function set verticalItemGap(value:Number):void
		{
			_verticalItemGap = value;
		}

		public function get layerNameGap():Number
		{
			return _layerNameGap;
		}

		public function set layerNameGap(value:Number):void
		{
			_layerNameGap = value;
		}

		public function get horizontalPatchGab():Number
		{
			return _horizontalPatchGab;
		}

		public function set horizontalPatchGab(value:Number):void
		{
			_horizontalPatchGab = value;
		}

		public function get horizontalItemGap():Number
		{
			return _horizontalItemGap;
		}

		public function set horizontalItemGap(value:Number):void
		{
			_horizontalItemGap = value;
		}

		public function get headingGap():Number
		{
			return _headingGap;
		}

		public function set headingGap(value:Number):void
		{
			_headingGap = value;
		}

		public function get groupGap():Number
		{
			return _groupGap;
		}

		public function set groupGap(value:Number):void
		{
			_groupGap = value;
		}

		public function get defaultPatchWidth():Number
		{
			return _defaultPatchWidth;
		}

		public function set defaultPatchWidth(value:Number):void
		{
			_defaultPatchWidth = value;
		}

		public function get defaultPatchHeight():Number
		{
			return _defaultPatchHeight;
		}

		public function set defaultPatchHeight(value:Number):void
		{
			_defaultPatchHeight = value;
		}

		public function get textGab():Number
		{
			return _textGab;
		}

		public function set textGab(value:Number):void
		{
			_textGab = value;
		}

		public function get titleGab():Number
		{
			return _titleGab;
		}

		public function set titleGab(value:Number):void
		{
			_titleGab = value;
		}

		public function get showTitle():Boolean
		{
			return _showTitle;
		}

		public function set showTitle(value:Boolean):void
		{
			_showTitle = value;
		}

		public function get titleSymbol():AgsJsonTextSymbol
		{
			return _titleSymbol;
		}

		public function set titleSymbol(value:AgsJsonTextSymbol):void
		{
			_titleSymbol = value;
		}

	}
}