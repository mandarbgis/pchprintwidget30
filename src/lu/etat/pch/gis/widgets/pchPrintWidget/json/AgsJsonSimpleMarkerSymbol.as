package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    import com.esri.ags.symbols.SimpleMarkerSymbol;
    /**
     *
     * @author schullto
     */
    public class AgsJsonSimpleMarkerSymbol extends AgsJsonSymbol {

        /**
         *
         * @param style
         * @param size
         * @param angle
         * @param xOffset
         * @param yOffset
         * @param outlineColor
         * @param outlineWidth
         */
        public function AgsJsonSimpleMarkerSymbol(style:String="x", size:Number=12, xOffset:Number=0, yOffset:Number=0, angle:Number=0, color:AgsJsonRGBColor=null, outline:AgsJsonSimpleLineSymbol=null) {
            type="esriSMS";
            this.style=style;
            this.size=size;
            this.angle=angle;
            this.xOffset=xOffset;
            this.yOffset=yOffset;
           	this.color=color;
            this.outline=outline;
        }

		private static var validStyles:Array=[ "esriSMSCircle", "esriSMSCross", "esriSMSDiamond", "esriSMSSquare", "esriSMSX" ];
		private static var esriStyles:Array=[ "circle", "cross", "diamond", "square", "x" ];

        public static function getFromObject(sms:SimpleMarkerSymbol):AgsJsonSimpleMarkerSymbol {
            if(sms==null)
                return null;
            var color:AgsJsonRGBColor=AgsJsonRGBColor.getObjectFromUINT(sms.color, sms.alpha);
            var outline:AgsJsonSimpleLineSymbol=AgsJsonSimpleLineSymbol.getFromObject(sms.outline);
            var retSym:AgsJsonSimpleMarkerSymbol=new AgsJsonSimpleMarkerSymbol(sms.style, sms.size, sms.xoffset, sms.yoffset, sms.angle, color, outline);
            return retSym;
        }

        private var _angle:Number;
        private var _color:AgsJsonRGBColor;
        private var _outline:AgsJsonSimpleLineSymbol;
        private var _size:Number;
        private var _style:String;
        private var _xOffset:Number;
        private var _yOffset:Number;

        /**
         *
         * @return
         */
        public function get angle():Number {
            return _angle;
        }

        /**
         *
         * @param value
         */
        public function set angle(value:Number):void {
            _angle=value;
        }

        public function get color():AgsJsonRGBColor {
            return _color;
        }

        public function set color(value:AgsJsonRGBColor):void {
            _color=value;
        }

        public function get outline():AgsJsonSimpleLineSymbol {
            return _outline;
        }

        public function set outline(value:AgsJsonSimpleLineSymbol):void {
            _outline=value;
        }

        /**
         *
         * @return
         */
        public function get size():Number {
            return _size;
        }

        /**
         *
         * @param value
         */
        public function set size(value:Number):void {
            _size=value;
        }

        /**
         *     esriSMSCircle = "circle";
         *     esriSMSSquare = "square";
         *     esriSMSCross = "cross";
         *     esriSMSX = "x";
         *     esriSMSDiamond = "diamond";
         * @return
         */
        public function get style():String {
            return _style;
        }

        /**
         *     esriSMSCircle = "circle";
         *     esriSMSSquare = "square";
         *     esriSMSCross = "cross";
         *     esriSMSX = "x";
         *     esriSMSDiamond = "diamond";
         * @param value
         */
        public function set style(value:String):void {
			if(value.indexOf("esriSMS")>=0)
				_style=value;
			else 
				_style=type+value.substring(0, 1).toUpperCase()+value.substring(1).toLowerCase();
            if(validStyles.indexOf(_style)<0) {
                trace("!!!!!! invalid style used for AgsJsonSimpleMarkerSymbol: "+value+" !!!!!!");
            }
        }

        /**
         *
         * @return
         */
        public function get xOffset():Number {
            return _xOffset;
        }

        /**
         *
         * @param value
         */
        public function set xOffset(value:Number):void {
            _xOffset=value;
        }

        /**
         *
         * @return
         */
        public function get yOffset():Number {
            return _yOffset;
        }

        /**
         *
         * @param value
         */
        public function set yOffset(value:Number):void {
            _yOffset=value;
        }
    }
}