package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    /**
     *
     * @author schullto
     */
    public class AgsJsonRGBColor {
        /**
         *
         * @param red
         * @param green
         * @param blue
         * @param alpha
         */
        public function AgsJsonRGBColor(red:Number=0, green:Number=0, blue:Number=0, alpha:Number=1) {
            this.red=red;
            this.green=green;
            this.blue=blue;
            if(isNaN(alpha))
                this.alpha=255;
            else
                this.alpha=alpha*255;
        }
		
		public function toUINT(): uint
		{
			return red << 16 | green << 8 | blue << 0;
		}

		public static function getObjectFromRGBhex(rgbHex:uint, alpha:Number=1):AgsJsonRGBColor {
			var rgbColor:AgsJsonRGBColor=new AgsJsonRGBColor();
			rgbColor.red =  (rgbHex & 0xff0000) >> 16;
			rgbColor.green =  (rgbHex & 0x00ff00) >> 8;
			rgbColor.blue =  (rgbHex & 0x0000ff) >> 0;
			return rgbColor;		
		}

		public static function getObjectFromUINT(color:uint, alpha:Number=1):AgsJsonRGBColor {
            var rgbColor:AgsJsonRGBColor=new AgsJsonRGBColor();
            var colorHexRep:String=color.toString(16);
            if(colorHexRep.length==6) {
                rgbColor.red=new uint("0x"+colorHexRep.substring(0, 2));
                rgbColor.green=new uint("0x"+colorHexRep.substring(2, 4));
                rgbColor.blue=new uint("0x"+colorHexRep.substring(4, 6));
            } else if(colorHexRep.length==4) {
                rgbColor.red=0;
                rgbColor.green=new uint("0x"+colorHexRep.substring(0, 2));
                rgbColor.blue=new uint("0x"+colorHexRep.substring(2, 4));
            } else if(colorHexRep.length==2) {
                rgbColor.red=0;
                rgbColor.green=0;
                rgbColor.blue=new uint("0x"+colorHexRep.substring(0, 2));
            } else {
                rgbColor.red=0;
                rgbColor.green=0;
                rgbColor.blue=0;
            }
            if(isNaN(alpha))
                rgbColor.alpha=255;
            else
                rgbColor.alpha=alpha*255;
            return rgbColor;
        }

        private var _alpha:Number;
        private var _blue:Number;
        private var _green:Number;
        private var _red:Number;

        /**
         * alpha rest representation 0..255
         * @return
         */
        public function get alpha():Number {
            return _alpha;
        }

        /**
         * alpha rest representation 0..255
         * @param value
         */
        public function set alpha(value:Number):void {
            _alpha=value;
        }

        /**
         *
         * @return
         */
        public function get blue():Number {
            return _blue;
        }

        /**
         *
         * @param value
         */
        public function set blue(value:Number):void {
            _blue=value;
        }

        /**
         *
         * @return
         */
        public function get green():Number {
            return _green;
        }

        /**
         *
         * @param value
         */
        public function set green(value:Number):void {
            _green=value;
        }

        /**
         *
         * @return
         */
        public function get red():Number {
            return _red;
        }

        /**
         *
         * @param value
         */
        public function set red(value:Number):void {
            _red=value;
        }
    }
}