package lu.etat.pch.gis.widgets.pchPrintWidget.components {
    import flash.events.Event;

    import mx.collections.ArrayCollection;
    import mx.events.FlexEvent;

    import spark.components.HSlider;

    public class ScaleSlider extends HSlider {
        private var _dataProvider:Array = null;

        private var _scaleValue:Number = 0;

        public function ScaleSlider() {
            super();
            this.minimum = 0;
            this.stepSize = 1;
            this.addEventListener(Event.CHANGE, valueChange);
            this.dataTipFormatFunction = dataTipLabel;
        }

        public function dataTipLabel(item:Object):String {
            return _dataProvider[item].value;
        }

        public function valueChange(event:Event):void {
            _scaleValue = _dataProvider[value].value;
            dispatchEvent(new Event("scaleValueChanged"));
        }

        [Bindable(event="dataProviderChanged")]
        public function get minimumScale():Number {
            return _dataProvider[0].value as Number;
        }

        [Bindable(event="dataProviderChanged")]
        public function get maximumScale():Number {
            return _dataProvider[_dataProvider.length - 1].value as Number;
        }

        public function getPreviousScale(scaleValue:Number, included:Boolean = true):Number {
            // Find slider scale value
            var result:Number = this.minimumScale;
            for each (var scale:Object in dataProvider) {
                if (included && scale.value <= scaleValue)
                    result = scale.value;
                else if (!included && scale.value < scaleValue)
                    result = scale.value;
                else
                    break;
            }
            return result
        }


        public function updateScaleValue(scaleValue:Number):void {
            _scaleValue = scaleValue;
            var i:int = 0;
            for each (var item:Object in _dataProvider) {
                if (item.value == scaleValue) {
                    value = i;
                    break;
                }
                i++;
            }
        }

        public function set scaleValue(scaleValue:Number):void {
            updateScaleValue(scaleValue);
            dispatchEvent(new Event("scaleValueChanged"));
        }

        [Bindable(event="scaleValueChanged")]
        public function get scaleValue():Number {
            return _scaleValue;
        }

        [Bindable(event="dataProviderChanged")]
        public function get dataProvider():Array {
            return _dataProvider;
        }

        public function set dataProvider(dataProvider:Array):void {
            _dataProvider = dataProvider;
            this.maximum = _dataProvider.length - 1;
            dispatchEvent(new Event("dataProviderChanged"));
        }

    }
}