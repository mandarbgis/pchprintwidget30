developed by tom@schuller.lu

PchPrintWidget:

Features:
 - basic user interface for the PChPrintSOE:
	 - full print over the rest api
	 - make print on "template" mapservice by keeping the layout
	 - multi-mapservices support
	 - add graphics on the map view positioned in map units
	 - add elements on the layout view positioned in paper units
	 - on each mapservice restart, the generated files will be deleted (cleanup)

